package com.proto.donation.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

import com.proto.donation.document.Donation;

public interface DonationRepository extends ReactiveMongoRepository<Donation, String>{

}
