package com.proto.donation.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

import com.proto.donation.document.User;

public interface UserRepository extends ReactiveMongoRepository<User, String>{

}
