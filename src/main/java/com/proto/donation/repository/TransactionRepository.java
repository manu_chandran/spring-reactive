package com.proto.donation.repository;

import java.util.Date;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.mongodb.repository.Tailable;

import com.proto.donation.document.Transaction;

import reactor.core.publisher.Flux;

public interface TransactionRepository extends ReactiveMongoRepository<Transaction, String>{
	
	 @Tailable
	 Flux<Transaction> findWithTailableCursorBy();

}
