package com.proto.donation.config;

import java.util.Collections;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;

import com.proto.donation.document.Transaction;
import com.proto.donation.repository.TransactionRepository;
import com.proto.donation.utils.DonationUtils;

import reactor.core.publisher.Flux;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebFlux;




@EnableSwagger2WebFlux
@Configuration
@PropertySources({
    @PropertySource("classpath:mongoconfig.properties")
})
public class DonationConfig{

	@Autowired
	private DonationUtils donationUtils;
	
    @Bean
    public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2)  
          .select()                                  
          .apis(RequestHandlerSelectors.any())              
          .paths(PathSelectors.any())                          
          .build()
          .apiInfo(apiInfo());                                           
    }
   
	@Bean
	public Random random() {
		return new Random();
	}
   
   private ApiInfo apiInfo() {
       ApiInfo apiInfo = new ApiInfo("Donation Reactive Applicaton", "Reactive Web Appication using Spring Webflux", "1.0", "Terms of service",
               new Contact("", "", ""), "", "", Collections.EMPTY_LIST);
       return apiInfo;
   }
   
  	
	@Bean
	public ApplicationRunner initialize(final ReactiveMongoTemplate mongoTemplate) {
		return args -> {

			donationUtils.initializeData(mongoTemplate);
			Flux<Transaction> transactionStream = donationUtils.createTransactionStream();
			transactionStream.subscribe();



		};
	}


	
	
	
}
