package com.proto.donation.document;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "donation")
public class Donation {
	
	@Id 
	private String id;
	
	@Field("donationid")
	private String donationID;
	
	@Field("donationname")
	private String donationName;
	
	@Field("donationtype")
	private String donationType;
	
	@Field("donationdescription")
	private String donationDescription;
	
	@Field("donationimage")
	private byte[] donationImage;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDonationID() {
		return donationID;
	}

	public void setDonationID(String donationID) {
		this.donationID = donationID;
	}

	public String getDonationName() {
		return donationName;
	}

	public void setDonationName(String donationName) {
		this.donationName = donationName;
	}

	public String getDonationType() {
		return donationType;
	}

	public void setDonationType(String donationType) {
		this.donationType = donationType;
	}

	public String getDonationDescription() {
		return donationDescription;
	}

	public void setDonationDescription(String donationDescription) {
		this.donationDescription = donationDescription;
	}

	public byte[] getDonationImage() {
		return donationImage;
	}

	public void setDonationImage(byte[] donationImage) {
		this.donationImage = donationImage;
	}
	
	
	
	
	
	
}
