package com.proto.donation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.proto.donation.document.User;
import com.proto.donation.service.UserService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("users")
public class UserController {

   @Autowired
   private UserService userService;
	
   @PostMapping
   Mono<Void> create(@RequestBody User userDTO) {
	   
	   return userService.createUser(userDTO);
   }
   
   @GetMapping()
   Flux<User> getAllUsers(){
	   return userService.findAllUsers();
   }
   
   
    @GetMapping("/check")
    String list1() {
        return "Welcome to spring 5";
    }
}
