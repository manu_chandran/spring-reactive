package com.proto.donation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.proto.donation.document.Donation;
import com.proto.donation.service.DonationService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("donations")
public class DonationController {

	@Autowired
	private DonationService donationService;

	@PostMapping()
	Mono<Void> createDonation(@RequestBody Donation donation) {

		return donationService.createDonation(donation);

	}


	@GetMapping()
	Flux<Donation> getAllDonations() {

		return donationService.findAllDonations();
	}
	
	
	@GetMapping(value = "/findone/{id}")
	Mono<Donation> getDonations(@PathVariable String id) {

		return donationService.findDonation(id);
	}
	
	
	@DeleteMapping(value = "/findone/{id}")
	Mono<Void> deleteDonation(@PathVariable String id) {

		return donationService.deleteDonation(id);

	}
	
	
	@PutMapping()
	Mono<Void> updateDonation(@RequestBody Donation donation) {

		return donationService.updateDonation(donation);

	}
	

}
