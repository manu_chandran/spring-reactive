package com.proto.donation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.proto.donation.document.Transaction;
import com.proto.donation.service.TransactionService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("transactions")
public class TransactionController {
	
	
	@Autowired
	private TransactionService transactionService;

	@PostMapping
	Mono<Void> create(@RequestBody Transaction transactionDTO) {
		return transactionService.createTransaction(transactionDTO);
	}

	@GetMapping(produces = MediaType.TEXT_EVENT_STREAM_VALUE)
	Flux<Transaction> getAllTransactions() {
		return transactionService.findAllTransactions();
	}
	
}
