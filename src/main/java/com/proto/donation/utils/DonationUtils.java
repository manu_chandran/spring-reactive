package com.proto.donation.utils;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.springframework.beans.BeanUtils;
import org.springframework.data.mongodb.core.CollectionOptions;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.stereotype.Component;

import com.proto.donation.document.Transaction;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@Component
public class DonationUtils {
	
	private static final Duration EVENT_INTERVAL = Duration.ofSeconds(10);
	
	private Random random =  new Random();

	private List<Transaction> transactionList = generateTransactionEntries();
	
	private ReactiveMongoTemplate mongoTemplate;
	
	
	public void initializeData(ReactiveMongoTemplate mongoTemplate) {
		this.mongoTemplate = mongoTemplate;
		this.mongoTemplate
        .dropCollection(Transaction.class)
        .then(mongoTemplate.createCollection(
        		Transaction.class, CollectionOptions.empty().size(104857600).capped())) 
        .block();
	}
	
	
	
	public Flux<Transaction> createTransactionStream() {
		return Flux.interval(EVENT_INTERVAL).flatMap(i -> insertTransactionEntries(generateTransaction(transactionList.get(random.nextInt(24) + 1))));
	}
	
		
	
	private Mono<Transaction> insertTransactionEntries(Transaction transaction){
		return Mono.just(transaction).flatMap(this.mongoTemplate::insert).log();
	}
		
	private Transaction generateTransaction(Transaction transaction) {
		Transaction tranObj = new Transaction();
		BeanUtils.copyProperties(transaction, tranObj,"id","transactionID");
		tranObj.setTransactionID(String.valueOf(random.nextInt(500000)));
		return tranObj;
	}
	
	public List<Transaction> generateTransactionEntries(){
		
		List<Transaction> transactionEntries = new ArrayList<>();
		
		Transaction transaction = new Transaction();
		transaction.setAddress("Dubai,UAE");
		transaction.setCardExpDate("0922");
		transaction.setCardNumber("4111111111111111");
		transaction.setCountry("United Arab Emirates");
		transaction.setDonationAmount(10);
		transaction.setDonationName("Where it is needed most");
		transaction.setDonationType("Corporations");
		transaction.setEmail("raj.kumar@gmail.com");
		transaction.setFirstName("Raj");
		transaction.setLastName("Kumar");
		transaction.setNameOnCard("Raj");
		transaction.setState("Dubai");
		transaction.setTransactionDate(new Date());
		transaction.setTransactionID(String.valueOf(random.nextInt(500000))) ;
		transaction.setZip("22190");
		
		transactionEntries.add(transaction);
		
		
		Transaction transaction1 = new Transaction();
		transaction1.setAddress("Dubai,UAE");
		transaction1.setCardExpDate("0922");
		transaction1.setCardNumber("4111111111111111");
		transaction1.setCountry("United Arab Emirates");
		transaction1.setDonationAmount(25);
		transaction1.setDonationName("Natural Calamities");
		transaction1.setDonationType("Foundations");
		transaction1.setEmail("honey.stephen@gmail.com");
		transaction1.setFirstName("Honey");
		transaction1.setLastName("Stephen");
		transaction1.setNameOnCard("Honey");
		transaction1.setState("Dubai");
		transaction1.setTransactionDate(new Date());
		transaction1.setTransactionID(String.valueOf(random.nextInt(500000))) ;
		transaction1.setZip("22190");
		
		transactionEntries.add(transaction1);
		
		
		Transaction transaction2 = new Transaction();
		transaction2.setAddress("Dubai,UAE");
		transaction2.setCardExpDate("0922");
		transaction2.setCardNumber("4111111111111111");
		transaction2.setCountry("United Arab Emirates");
		transaction2.setDonationAmount(50);
		transaction2.setDonationName("Habitat for Humanity");
		transaction2.setDonationType("Individuals");
		transaction2.setEmail("alex_joseph@yahoo.com");
		transaction2.setFirstName("Alex");
		transaction2.setLastName("Joesph");
		transaction2.setNameOnCard("Alex");
		transaction2.setState("Dubai");
		transaction2.setTransactionDate(new Date());
		transaction2.setTransactionID(String.valueOf(random.nextInt(500000))) ;
		transaction2.setZip("22190");
		
		transactionEntries.add(transaction2);
		
		
		Transaction transaction3 = new Transaction();
		transaction3.setAddress("Dubai,UAE");
		transaction3.setCardExpDate("0922");
		transaction3.setCardNumber("4111111111111111");
		transaction3.setCountry("United Arab Emirates");
		transaction3.setDonationAmount(100);
		transaction3.setDonationName("Habitat for Humanity");
		transaction3.setDonationType("Individuals");
		transaction3.setEmail("nandhan.vinu@gmail.com");
		transaction3.setFirstName("Nandhana");
		transaction3.setLastName("Vinu");
		transaction3.setNameOnCard("Nandhana");
		transaction3.setState("Dubai");
		transaction3.setTransactionDate(new Date());
		transaction3.setTransactionID(String.valueOf(random.nextInt(500000))) ;
		transaction3.setZip("22190");
		
		transactionEntries.add(transaction3);
		
		
		
		Transaction transaction4 = new Transaction();
		transaction4.setAddress("Dubai,UAE");
		transaction4.setCardExpDate("0922");
		transaction4.setCardNumber("4111111111111111");
		transaction4.setCountry("United Arab Emirates");
		transaction4.setDonationAmount(10);
		transaction4.setDonationName("Where it is needed most");
		transaction4.setDonationType("Corporations");
		transaction4.setEmail("devi.shyamala@gmail.com");
		transaction4.setFirstName("Devi");
		transaction4.setLastName("Shyamala");
		transaction4.setNameOnCard("Devi");
		transaction4.setState("Dubai");
		transaction4.setTransactionDate(new Date());
		transaction4.setTransactionID(String.valueOf(random.nextInt(500000))) ;
		transaction4.setZip("22190");
		
		transactionEntries.add(transaction4);
		
		
		
		Transaction transaction5 = new Transaction();
		transaction5.setAddress("Dubai,UAE");
		transaction5.setCardExpDate("0922");
		transaction5.setCardNumber("4111111111111111");
		transaction5.setCountry("United Arab Emirates");
		transaction5.setDonationAmount(25);
		transaction5.setDonationName("Natural Calamities");
		transaction5.setDonationType("Foundations");
		transaction5.setEmail("msanjay@gmail.com");
		transaction5.setFirstName("Maidhily");
		transaction5.setLastName("Sanjay");
		transaction5.setNameOnCard("Maidhily");
		transaction5.setState("Dubai");
		transaction5.setTransactionDate(new Date());
		transaction5.setTransactionID(String.valueOf(random.nextInt(500000))) ;
		transaction5.setZip("22190");
		
		transactionEntries.add(transaction5);
		
		
		
		Transaction transaction6 = new Transaction();
		transaction6.setAddress("Dubai,UAE");
		transaction6.setCardExpDate("0922");
		transaction6.setCardNumber("4111111111111111");
		transaction6.setCountry("United Arab Emirates");
		transaction6.setDonationAmount(50);
		transaction6.setDonationName("Habitat for Humanity");
		transaction6.setDonationType("Individuals");
		transaction6.setEmail("arjun.nair@gmail.com");
		transaction6.setFirstName("Arjun");
		transaction6.setLastName("Nair");
		transaction6.setNameOnCard("Arjun");
		transaction6.setState("Dubai");
		transaction6.setTransactionDate(new Date());
		transaction6.setTransactionID(String.valueOf(random.nextInt(500000))) ;
		transaction6.setZip("22190");
		
		transactionEntries.add(transaction6);
		
		
		
		Transaction transaction7 = new Transaction();
		transaction7.setAddress("Dubai,UAE");
		transaction7.setCardExpDate("0922");
		transaction7.setCardNumber("4111111111111111");
		transaction7.setCountry("United Arab Emirates");
		transaction7.setDonationAmount(100);
		transaction7.setDonationName("Disater Relief");
		transaction7.setDonationType("Foundations");
		transaction7.setEmail("rony_mathew@yahoo.com");
		transaction7.setFirstName("Rony");
		transaction7.setLastName("Mathew");
		transaction7.setNameOnCard("Sam");
		transaction7.setState("Dubai");
		transaction7.setTransactionDate(new Date());
		transaction7.setTransactionID(String.valueOf(random.nextInt(500000))) ;
		transaction7.setZip("22190");
		
		transactionEntries.add(transaction7);
		
		
		
		Transaction transaction8 = new Transaction();
		transaction8.setAddress("Dubai,UAE");
		transaction8.setCardExpDate("0922");
		transaction8.setCardNumber("4111111111111111");
		transaction8.setCountry("United Arab Emirates");
		transaction8.setDonationAmount(10);
		transaction8.setDonationName("Disater Relief");
		transaction8.setDonationType("Foundations");
		transaction8.setEmail("main.javed@gmail.com");
		transaction8.setFirstName("Main");
		transaction8.setLastName("Javed");
		transaction8.setNameOnCard("Main");
		transaction8.setState("Dubai");
		transaction8.setTransactionDate(new Date());
		transaction8.setTransactionID(String.valueOf(random.nextInt(500000))) ;
		transaction8.setZip("22190");
		
		transactionEntries.add(transaction8);
		
		
		
		Transaction transaction9 = new Transaction();
		transaction9.setAddress("Dubai,UAE");
		transaction9.setCardExpDate("0922");
		transaction9.setCardNumber("4111111111111111");
		transaction9.setCountry("United Arab Emirates");
		transaction9.setDonationAmount(25);
		transaction9.setDonationName("Natural Calamities");
		transaction9.setDonationType("Foundations");
		transaction9.setEmail("mustafaimran@gmail.com");
		transaction9.setFirstName("Mustafa");
		transaction9.setLastName("Imran");
		transaction9.setNameOnCard("Mustafa");
		transaction9.setState("Dubai");
		transaction9.setTransactionDate(new Date());
		transaction9.setTransactionID(String.valueOf(random.nextInt(500000))) ;
		transaction9.setZip("22190");
		
		transactionEntries.add(transaction9);
		
		
		
		Transaction transaction10 = new Transaction();
		transaction10.setAddress("Dubai,UAE");
		transaction10.setCardExpDate("0922");
		transaction10.setCardNumber("4111111111111111");
		transaction10.setCountry("United Arab Emirates");
		transaction10.setDonationAmount(100);
		transaction10.setDonationName("Where it is needed most");
		transaction10.setDonationType("Corporations");
		transaction10.setEmail("anithamenon@gmail.com");
		transaction10.setFirstName("Anitha");
		transaction10.setLastName("Menon");
		transaction10.setNameOnCard("Anitha");
		transaction10.setState("Dubai");
		transaction10.setTransactionDate(new Date());
		transaction10.setTransactionID(String.valueOf(random.nextInt(500000))) ;
		transaction10.setZip("22190");
		
		transactionEntries.add(transaction10);
		
		
		
		Transaction transaction11 = new Transaction();
		transaction11.setAddress("Dubai,UAE");
		transaction11.setCardExpDate("0922");
		transaction11.setCardNumber("4111111111111111");
		transaction11.setCountry("United Arab Emirates");
		transaction11.setDonationAmount(50);
		transaction11.setDonationName("Disater Relief");
		transaction11.setDonationType("Foundations");
		transaction11.setEmail("peterjohn@gmail.com");
		transaction11.setFirstName("Peter");
		transaction11.setLastName("John");
		transaction11.setNameOnCard("Peter");
		transaction11.setState("Dubai");
		transaction11.setTransactionDate(new Date());
		transaction11.setTransactionID(String.valueOf(random.nextInt(500000))) ;
		transaction11.setZip("22190");
		
		transactionEntries.add(transaction11);
		
		
		
		Transaction transaction12 = new Transaction();
		transaction12.setAddress("Dubai,UAE");
		transaction12.setCardExpDate("0922");
		transaction12.setCardNumber("4111111111111111");
		transaction12.setCountry("United Arab Emirates");
		transaction12.setDonationAmount(25);
		transaction12.setDonationName("Natural Calamities");
		transaction12.setDonationType("Foundations");
		transaction12.setEmail("rohitkumar@gmail.com");
		transaction12.setFirstName("Rohit");
		transaction12.setLastName("Kumar");
		transaction12.setNameOnCard("Kumar");
		transaction12.setState("Dubai");
		transaction12.setTransactionDate(new Date());
		transaction12.setTransactionID(String.valueOf(random.nextInt(500000))) ;
		transaction12.setZip("22190");
		
		transactionEntries.add(transaction12);
		
		
		
		Transaction transaction13 = new Transaction();
		transaction13.setAddress("Dubai,UAE");
		transaction13.setCardExpDate("0922");
		transaction13.setCardNumber("4111111111111111");
		transaction13.setCountry("United Arab Emirates");
		transaction13.setDonationAmount(10);
		transaction13.setDonationName("Where it is needed most");
		transaction13.setDonationType("Corporations");
		transaction13.setEmail("henry.stephen@gmail.com");
		transaction13.setFirstName("Henry");
		transaction13.setLastName("Stephen");
		transaction13.setNameOnCard("Henry");
		transaction13.setState("Dubai");
		transaction13.setTransactionDate(new Date());
		transaction13.setTransactionID(String.valueOf(random.nextInt(500000))) ;
		transaction13.setZip("22190");
		
		transactionEntries.add(transaction13);
		
		
		
		Transaction transaction14 = new Transaction();
		transaction14.setAddress("Dubai,UAE");
		transaction14.setCardExpDate("0922");
		transaction14.setCardNumber("4111111111111111");
		transaction14.setCountry("United Arab Emirates");
		transaction14.setDonationAmount(100);
		transaction14.setDonationName("Natural Calamities");
		transaction14.setDonationType("Foundations");
		transaction14.setEmail("abhilash.george@gmail.com");
		transaction14.setFirstName("Abhilash");
		transaction14.setLastName("George");
		transaction14.setNameOnCard("Abhilash");
		transaction14.setState("Dubai");
		transaction14.setTransactionDate(new Date());
		transaction14.setTransactionID(String.valueOf(random.nextInt(500000))) ;
		transaction14.setZip("22190");
		
		transactionEntries.add(transaction14);
		
		
		
		Transaction transaction15 = new Transaction();
		transaction15.setAddress("Dubai,UAE");
		transaction15.setCardExpDate("0922");
		transaction15.setCardNumber("4111111111111111");
		transaction15.setCountry("United Arab Emirates");
		transaction15.setDonationAmount(10);
		transaction15.setDonationName("Where it is needed most");
		transaction15.setDonationType("Corporations");
		transaction15.setEmail("aneena.edward@gmail.com");
		transaction15.setFirstName("Aneena");
		transaction15.setLastName("Edward");
		transaction15.setNameOnCard("Aneena");
		transaction15.setState("Dubai");
		transaction15.setTransactionDate(new Date());
		transaction15.setTransactionID(String.valueOf(random.nextInt(500000))) ;
		transaction15.setZip("22190");
		
		transactionEntries.add(transaction15);
		
		
		
		Transaction transaction16 = new Transaction();
		transaction16.setAddress("Dubai,UAE");
		transaction16.setCardExpDate("0922");
		transaction16.setCardNumber("4111111111111111");
		transaction16.setCountry("United Arab Emirates");
		transaction16.setDonationAmount(25);
		transaction16.setDonationName("Disater Relief");
		transaction16.setDonationType("Foundations");
		transaction16.setEmail("lakshmy.harshan@gmail.com");
		transaction16.setFirstName("Lakshmy");
		transaction16.setLastName("Harshan");
		transaction16.setNameOnCard("Sam");
		transaction16.setState("Dubai");
		transaction16.setTransactionDate(new Date());
		transaction16.setTransactionID(String.valueOf(random.nextInt(500000))) ;
		transaction16.setZip("22190");
		
		transactionEntries.add(transaction16);
		
		
		
		Transaction transaction17 = new Transaction();
		transaction17.setAddress("Dubai,UAE");
		transaction17.setCardExpDate("0922");
		transaction17.setCardNumber("4111111111111111");
		transaction17.setCountry("United Arab Emirates");
		transaction17.setDonationAmount(50);
		transaction17.setDonationName("Habitat for Humanity");
		transaction17.setDonationType("Individuals");
		transaction17.setEmail("rammohan@gmail.com");
		transaction17.setFirstName("Ram");
		transaction17.setLastName("Mohan");
		transaction17.setNameOnCard("Ram");
		transaction17.setState("Dubai");
		transaction17.setTransactionDate(new Date());
		transaction17.setTransactionID(String.valueOf(random.nextInt(500000))) ;
		transaction17.setZip("22190");
		
		transactionEntries.add(transaction17);
		
		
		Transaction transaction18 = new Transaction();
		transaction18.setAddress("Dubai,UAE");
		transaction18.setCardExpDate("0922");
		transaction18.setCardNumber("4111111111111111");
		transaction18.setCountry("United Arab Emirates");
		transaction18.setDonationAmount(25);
		transaction18.setDonationName("Natural Calamities");
		transaction18.setDonationType("Individuals");
		transaction18.setEmail("deeepa.shyamala@bradycorp.com");
		transaction18.setFirstName("Deepa");
		transaction18.setLastName("Shyamala");
		transaction18.setNameOnCard("Deepa S");
		transaction18.setState("Dubai");
		transaction18.setTransactionDate(new Date());
		transaction18.setTransactionID(String.valueOf(random.nextInt(500000))) ;
		transaction18.setZip("22190");
		
		transactionEntries.add(transaction18);
		
		
		
		Transaction transaction19 = new Transaction();
		transaction19.setAddress("Dubai,UAE");
		transaction19.setCardExpDate("0922");
		transaction19.setCardNumber("4111111111111111");
		transaction19.setCountry("United Arab Emirates");
		transaction19.setDonationAmount(10);
		transaction19.setDonationName("Disater Relief");
		transaction19.setDonationType("Foundations");
		transaction19.setEmail("josemg@hotmail.com");
		transaction19.setFirstName("Jose");
		transaction19.setLastName("M G");
		transaction19.setNameOnCard("Jose");
		transaction19.setState("Dubai");
		transaction19.setTransactionDate(new Date());
		transaction19.setTransactionID(String.valueOf(random.nextInt(500000))) ;
		transaction19.setZip("22190");
		
		transactionEntries.add(transaction19);
		
		
		
		Transaction transaction20 = new Transaction();
		transaction20.setAddress("Dubai,UAE");
		transaction20.setCardExpDate("0922");
		transaction20.setCardNumber("4111111111111111");
		transaction20.setCountry("United Arab Emirates");
		transaction20.setDonationAmount(25);
		transaction20.setDonationName("Habitat for Humanity");
		transaction20.setDonationType("Individuals");
		transaction20.setEmail("rahul.s@yahoo.com");
		transaction20.setFirstName("Rahul");
		transaction20.setLastName("S");
		transaction20.setNameOnCard("Rahul");
		transaction20.setState("Dubai");
		transaction20.setTransactionDate(new Date());
		transaction20.setTransactionID(String.valueOf(random.nextInt(500000))) ;
		transaction20.setZip("22190");
		
		transactionEntries.add(transaction20);
		
		
		Transaction transaction21 = new Transaction();
		transaction21.setAddress("Dubai,UAE");
		transaction21.setCardExpDate("0922");
		transaction21.setCardNumber("4111111111111111");
		transaction21.setCountry("United Arab Emirates");
		transaction21.setDonationAmount(50);
		transaction21.setDonationName("Natural Calamities");
		transaction21.setDonationType("Foundations");
		transaction21.setEmail("franklinantony@gmail.com");
		transaction21.setFirstName("Franklin");
		transaction21.setLastName("Antony");
		transaction21.setNameOnCard("Franklin");
		transaction21.setState("Dubai");
		transaction21.setTransactionDate(new Date());
		transaction21.setTransactionID(String.valueOf(random.nextInt(500000))) ;
		transaction21.setZip("22190");
		
		transactionEntries.add(transaction21);
		
		
		
		Transaction transaction22 = new Transaction();
		transaction22.setAddress("Dubai,UAE");
		transaction22.setCardExpDate("0922");
		transaction22.setCardNumber("4111111111111111");
		transaction22.setCountry("United Arab Emirates");
		transaction22.setDonationAmount(100);
		transaction22.setDonationName("Natural Calamities");
		transaction22.setDonationType("Foundations");
		transaction22.setEmail("samedward@gmail.com");
		transaction22.setFirstName("Mohammed");
		transaction22.setLastName("Thaiseer");
		transaction22.setNameOnCard("Mohammed");
		transaction22.setState("Dubai");
		transaction22.setTransactionDate(new Date());
		transaction22.setTransactionID(String.valueOf(random.nextInt(500000))) ;
		transaction22.setZip("22190");
		
		transactionEntries.add(transaction22);
		
		
		Transaction transaction23 = new Transaction();
		transaction23.setAddress("Dubai,UAE");
		transaction23.setCardExpDate("0922");
		transaction23.setCardNumber("4111111111111111");
		transaction23.setCountry("United Arab Emirates");
		transaction23.setDonationAmount(100);
		transaction23.setDonationName("Where it is needed most");
		transaction23.setDonationType("Corporations");
		transaction23.setEmail("samedward@gmail.com");
		transaction23.setFirstName("Jiffy");
		transaction23.setLastName("Shams");
		transaction23.setNameOnCard("Jiffy");
		transaction23.setState("Dubai");
		transaction23.setTransactionDate(new Date());
		transaction23.setTransactionID(String.valueOf(random.nextInt(500000))) ;
		transaction23.setZip("22190");
		
		transactionEntries.add(transaction23);
		
		
		
		Transaction transaction24 = new Transaction();
		transaction24.setAddress("Dubai,UAE");
		transaction24.setCardExpDate("0922");
		transaction24.setCardNumber("4111111111111111");
		transaction24.setCountry("United Arab Emirates");
		transaction24.setDonationAmount(50);
		transaction24.setDonationName("Disater Relief");
		transaction24.setDonationType("Foundations");
		transaction24.setEmail("aji.s@gmail.com");
		transaction24.setFirstName("Aji");
		transaction24.setLastName("Sheraf");
		transaction24.setNameOnCard("Aji");
		transaction24.setState("Dubai");
		transaction24.setTransactionDate(new Date());
		transaction24.setTransactionID(String.valueOf(random.nextInt(500000))) ;
		transaction24.setZip("22190");
		
		transactionEntries.add(transaction24);
		
		
		Transaction transaction25 = new Transaction();
		transaction25.setAddress("Dubai,UAE");
		transaction25.setCardExpDate("0922");
		transaction25.setCardNumber("4111111111111111");
		transaction25.setCountry("United Arab Emirates");
		transaction25.setDonationAmount(25);
		transaction25.setDonationName("Habitat for Humanity");
		transaction25.setDonationType("Individuals");
		transaction25.setEmail("samedward@gmail.com");
		transaction25.setFirstName("Saji");
		transaction25.setLastName("G");
		transaction25.setNameOnCard("Saji");
		transaction25.setState("Dubai");
		transaction25.setTransactionDate(new Date());
		transaction25.setTransactionID(String.valueOf(random.nextInt(500000))) ;
		transaction25.setZip("22190");
		
		transactionEntries.add(transaction25);
		
		return transactionEntries;
	}


	public Date geFromDate(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, -1);
		return calendar.getTime();
	}
}
