package com.proto.donation.service;

import com.proto.donation.document.Donation;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface DonationService {
	
	public Mono<Void> createDonation(Donation donationDTO);
	
	public Flux<Donation> findAllDonations();
	
	public Mono<Donation> findDonation(String id);
	
	public Mono<Void> deleteDonation(String id);
	
	public Mono<Void> updateDonation(Donation donationDTO);
 	

}
