package com.proto.donation.service;

import com.proto.donation.document.User;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface UserService {
	
	public Mono<Void> createUser(User userDTO);
	
	public Flux<User> findAllUsers();
	
	
	
	

}
