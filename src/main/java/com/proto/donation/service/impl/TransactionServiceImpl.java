package com.proto.donation.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proto.donation.document.Transaction;
import com.proto.donation.repository.TransactionRepository;
import com.proto.donation.service.TransactionService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class TransactionServiceImpl implements TransactionService{
	
	@Autowired
	private TransactionRepository transactionRepository;

	@Override
	public Mono<Void> createTransaction(Transaction transaction) {
		return transactionRepository.save(transaction).then();
	}

	@Override
	public Flux<Transaction> findAllTransactions() {
		return transactionRepository.findWithTailableCursorBy();
	}

	@Override
	public Flux<Transaction> findAllTransactionsForDateRange(Date fromDate,Date toDate) {
		return Flux.empty();
	}
	

}
