package com.proto.donation.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proto.donation.document.User;
import com.proto.donation.repository.UserRepository;
import com.proto.donation.service.UserService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class UserServiceImpl implements UserService{
	
	@Autowired
	private UserRepository userRepository;	

	@Override
	public Mono<Void> createUser(User user) {
		return userRepository.save(user).then();
	}

	@Override
	public Flux<User> findAllUsers() {
		return userRepository.findAll();
	}

}
