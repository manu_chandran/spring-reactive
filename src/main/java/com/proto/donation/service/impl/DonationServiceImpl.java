package com.proto.donation.service.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proto.donation.document.Donation;
import com.proto.donation.repository.DonationRepository;
import com.proto.donation.service.DonationService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class DonationServiceImpl implements DonationService {
	
	@Autowired
	private DonationRepository donationRepository;

	@Override
	public Mono<Void> createDonation(Donation donation) {
		return donationRepository.save(donation).then();
	}

	@Override
	public Flux<Donation> findAllDonations() {
		return donationRepository.findAll();
	}

	@Override
	public Mono<Donation> findDonation(String id) {
		return donationRepository.findById(id);
	}

	@Override
	public Mono<Void> deleteDonation(String id) {
		return donationRepository.deleteById(id);
	}

	@Override
	public Mono<Void> updateDonation(Donation updatedDonation) {
		return donationRepository.findById(updatedDonation.getId()).flatMap(donation ->{
			BeanUtils.copyProperties(updatedDonation, donation, "id","donationImage");
			return donationRepository.save(donation).then();
		});
	}

}
