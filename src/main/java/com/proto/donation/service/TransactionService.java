package com.proto.donation.service;

import java.util.Date;

import com.proto.donation.document.Transaction;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface TransactionService {
	
	public Mono<Void> createTransaction(Transaction transaction);
	
	public Flux<Transaction> findAllTransactions();
	
	public Flux<Transaction> findAllTransactionsForDateRange(Date fromDate,Date toDate);
	

}
