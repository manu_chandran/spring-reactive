package com.proto.donations;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import com.proto.donation.DonationApplication;
import com.proto.donation.document.Donation;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.test.StepVerifier;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.springframework.mock.web.MockMultipartFile;

@RunWith(SpringRunner.class)
//@SpringBootTest(classes = DonationApplication.class)
public class DonationApplicationTests {

	//@Test
	public void contextLoads() {
	}

	
	
	
	//@Test
	public void testDonationUpload() {
		WebClient client2 = WebClient.create("http://localhost:8080");
		
		Donation donation = new Donation();
		donation.setDonationDescription("test donation");
		donation.setDonationID("3111313");
		donation.setDonationName("My Donation");
		donation.setDonationType("Special");
		
//		MultipartBodyBuilder bodyBuilder = new MultipartBodyBuilder();
//		bodyBuilder.part("file", new ClassPathResource("Capture.JPG"));
//		bodyBuilder.part("donation", donation);

		
	

		Path path = Paths.get("C:\\Users\\c_manu.shyamala\\Desktop\\Capture.JPG");
		byte[] content = null;
		try {
		    content = Files.readAllBytes(path);
		} catch (final IOException e) {
			e.printStackTrace();
		}
		donation.setDonationImage(content);
		
		client2.post().uri("/donations").accept(MediaType.APPLICATION_JSON)
        .body(BodyInserters.fromObject(donation))
        .exchange().block();
		
	}
	
	@Test
	public void firstMethod() {
		
		
		
		Flux<Player> playerFlux = Flux
				.just("Michael Jordan", "Scottie Pippen", "Steve Kerr")
				.map(n -> {
				String[] split = n.split("\\s");
				return new Player(split[0], split[1]);
				});
				StepVerifier.create(playerFlux)
				.expectNext(new Player("Michael", "Jordan"))
				.expectNext(new Player("Scottie", "Pippen"))
				.expectNext(new Player("Steve", "Kerr"))
				.verifyComplete();
		
	}

	@Test
	public void flatMap() {
//		Flux<Player> playerFlux = Flux.just("Michael Jordan", "Scottie Pippen", "Steve Kerr")
//				.flatMap(n -> Mono.just(n));
		
		
		Flux<Player> playerFlux =  Flux.just("Michael Jordan", "Scottie Pippen", "Steve Kerr")
		.flatMap(n -> Mono.just(n)).map(p -> new Player(p,p));
				
						
//						.map(p -> {
//					String[] split = p.split("\\s");
//					return new Player(split[0], split[1]);
//				}));
		
		
		List<Player> playerList = Arrays.asList(new Player("Michael", "Jordan"), new Player("Scottie", "Pippen"),
				new Player("Steve", "Kerr"));
		StepVerifier.create(playerFlux).expectNextMatches(p -> playerList.contains(p))
				.expectNextMatches(p -> playerList.contains(p)).expectNextMatches(p -> playerList.contains(p))
				.verifyComplete();
		}
	
	
}
